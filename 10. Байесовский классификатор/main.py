#mystem class1.txt out_class1.txt -l -d -i -c


import fileinput
import re
import operator
import sys
from collections import Counter
from math import log

WORD = 'язык'
use_morph = True

test_ = (word for line in fileinput.input('out_example.txt',openhook=fileinput.hook_encoded("utf8"))
    for word in re.findall(r'\{[\w|=|,]*\}', line.casefold()))

test = []
for word in test_:
    a = word.find('=')
    b = word[a+1:].find('=')
    c = word[a+1:].find(',')
    d = min(b, c)
    if c==-1:
        d = b
    if b==-1:
        d = c
    test.append([word[1:a], word[a+1:d+a+1]])


lines1 = [line for line in fileinput.input('out_class1.txt',openhook=fileinput.hook_encoded("utf8"))]
lines_mod1 = [re.findall(r'\{[\w|=|,]*\}', line.casefold()) for line in lines1]

collection1 = []
for line in lines_mod1:
    example = []
    for word in line:
        a = word.find('=')
        b = word[a+1:].find('=')
        c = word[a+1:].find(',')
        d = min(b, c)
        if c==-1:
            d = b
        if b==-1:
            d = c
        example.append([word[1:a], word[a+1:d+a+1]])
    collection1.append(example)

lines2 = [line for line in fileinput.input('out_class2.txt',openhook=fileinput.hook_encoded("utf8"))]
lines_mod2 = [re.findall(r'\{[\w|=|,]*\}', line.casefold()) for line in lines2]

collection2 = []
for line in lines_mod2:
    example = []
    for word in line:
        a = word.find('=')
        b = word[a+1:].find('=')
        c = word[a+1:].find(',')
        d = min(b, c)
        if c==-1:
            d = b
        if b==-1:
            d = c
        example.append([word[1:a], word[a+1:d+a+1]])
    collection2.append(example)

documents = []

for example in collection1:
    for i in range(0, len(example)):
        if example[i][0] == WORD:
            document = []
            if i-1 > 0:
                if use_morph:
                    document.append(example[i-1][1])
                document.append(example[i-1][0])
            if i - 2 > 0:
                if use_morph:
                    document.append(example[i - 2][1])
                document.append(example[i - 2][0])
            if i + 1 < len(example):
                if use_morph:
                    document.append(example[i + 1][1])
                document.append(example[i + 1][0])
            if i + 2 < len(example):
                if use_morph:
                    document.append(example[i + 2][1])
                document.append(example[i + 2][0])
            documents.append((document, 1))

for example in collection2:
    for i in range(0, len(example)):
        if example[i][0] == WORD:
            document = []
            if i-1 > 0:
                if use_morph:
                    document.append(example[i-1][1])
                document.append(example[i-1][0])
            if i - 2 > 0:
                if use_morph:
                    document.append(example[i - 2][1])
                document.append(example[i - 2][0])
            if i + 1 < len(example):
                if use_morph:
                    document.append(example[i + 1][1])
                document.append(example[i + 1][0])
            if i + 2 < len(example):
                if use_morph:
                    document.append(example[i + 2][1])
                document.append(example[i + 2][0])
            documents.append((document, 2))

doc_test = []
for i in range(0, len(test)):
    if test[i][0] == WORD:
        document = []
        if i-1 > 0:
            if use_morph:
                document.append(test[i-1][1])
            document.append(test[i-1][0])
        if i - 2 > 0:
            if use_morph:
                document.append(test[i - 2][1])
            document.append(test[i - 2][0])
        if i + 1 < len(test):
            if use_morph:
                document.append(test[i + 1][1])
            document.append(test[i + 1][0])
        if i + 2 < len(test):
            if use_morph:
                document.append(test[i + 2][1])
            document.append(test[i + 2][0])
        doc_test.append(document)

V = set()

for document in documents:
    for mean in document[0]:
        V.add(mean)

N = len(documents)

N_ = {}

for document in documents:
    if document[1] in N_:
        N_[document[1]] = N_[document[1]] + 1
    else:
        N_[document[1]] = 1


prior = {}
N__ = {}
condprob = {}
for c in N_.keys():
    prior[c] = N_[c]/N
    for t in V:
        N__[c, t] = 0
        for document in documents:
            if document[1] == c and t in document[0]:
                N__[c, t] = N__[c, t] + 1
    for t in V:
        deriv = 0
        for t_ in V:
            deriv = deriv + N__[c, t_]+1
        condprob[t, c] = (N__[c, t]+1)/deriv


W = doc_test[0]

score = {}
for c in N_.keys():
    score[c] = log(prior[c])
    for t in W:
        if t in V:
            score[c] = score[c]+log(condprob[t, c])

print(score)